console.log("JavaScript - Array Manipulation");

//Array Methods
//JS has built-in functions and methods for arrays
//This allows us to manipu;ate and access array items

//Mutator Methods

/*

	Mutator Methods are functions that "mutate" or change an array after they are created
	These methods manipuate the original array performing various tasks such as adding and removing elements

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

//push
	/* adds an element in the end of an array and returns the array length
		
		Syntax:
		arrayName.push();

	*/
	console.log("Current Array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits); //Mango is added at the end of the array

	fruits.push('Avocado', 'Guava');
	console.log("Mutated array from push method: ");
	console.log(fruits); 


//pop 

	/*

		Removes the last element in an array and returns the removed element
		Syntax:

		arrayName.pop();


	*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method: ");
	console.log(fruits); 

	let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro'];

	/*
		Mini A.
		Create a function which will remove the last person in the array
		Log the ghostFighter array in the console
	*/

	function updatedFighters(){
		let removedFighter = ghostFighters.pop()
		
	}

	updatedFighters();
	console.log(ghostFighters);


	//unshift()

	/* 	
		adds one or move elements at the beginning of an array
		Syntax
			arrayName.unshift(elementA);
			arrayName.unshift(elementA, elementB);

	*/
	
		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method: ");
		console.log(fruits);//

	//shift()
	/*
		removes an element at the beginning of an array AND returns the removed element

		Syntax:
			arrayName.shift();		

	*/		

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);	
	console.log("Mutated array from shift method: ");
	console.log(fruits);

	//splice
	/*
		simultaneously removes elements from a specified index number and adds the elements

		Syntax
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

	*/	

	fruits.splice(1,2,'Lime', 'Cherry');
	console.log("Mutated array from splice method: ");
	console.log(fruits);

	//sort
	//A-Z
	fruits.sort();
	console.log("Mutated array from sort method: ");
	console.log(fruits);
	
	//reverse
	//Z-A
	fruits.reverse();
	console.log("Mutated array from reverse method: ");
	console.log(fruits);
	

	//Non-Mutator Methods
	/*
		Non-Mutator methods are functions that do not modify or change an array after they are created
		-these methods do not manipulate the original array performing various taks such as returning element sfrom an array and combining arrays and printing the output
	*/

	let countries = ['US', 'PH','CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	//indexOf()

	/*
		returns of the index number of the first matching element found in an array
		--if no match is found, the result is -1
		-the search process will be done from the first element processing to the last element
		Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);

	*/

	let firstIndex = countries.indexOf('PH');
	console.log("Result of indexOf method: " + firstIndex);//1
	let firstExample = countries.indexOf('ph');
	console.log("Result of indexOf method: " + firstExample);//-1
	let invalidCountry = countries.indexOf('BR');
	console.log("Result of indexOf method: " + invalidCountry);//1




	//lastIndexOf()

	/*
		returns of the index number of the last matching element found in an array
		
		-the search process will be done from the last element processing to the first element
		Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);

	*/

	let lastIndex = countries.lastIndexOf('PH', 3);
	console.log("Result of lastIndexOf method: " + lastIndex);//1	

	//slice()

	/*
		-portions/slices elements from an array AND returns a new array
		-syntax
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex,endingIndex);	
	*/

		console.log(countries);

		let sliceArrayA = countries.slice(2);
		console.log('Results from slice method: ');
		console.log(sliceArrayA);
		console.log(countries);

		let sliceArrayB = countries.slice(2,4);
		console.log('Results from slice method: ');
		console.log(sliceArrayB);//'CAN', 'SG' //[02, 03]

		let sliceArrayC = countries.slice(-3);
		console.log('Results from slice method: ');
		console.log(sliceArrayC); //'PH', 'FR', 'DE' /last 3 index

	//toString()

		/*

			returns an array as a string separated by commas,
			Syntax
			arrayName.toString();

		*/

		let stringArray = ghostFighters.toString();
		console.log("Results from toString method: ");
		console.log(stringArray);
		console.log(typeof stringArray);

	//concat()
		/*
			combine two arrays and returns the combined results
			Syntax
			arrayA.concat(arrayB);
			arrayA.concat(elementA);

		*/

		let taskArrayA = ['drink html', 'eat JavaScript'];
		let taskArrayB = ['inhale css', 'breathe react'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log('Result from concat method: ');
		console.log(tasks);
		//let stringArray1 = tasks.toString();
		//console.log(stringArray1);

		//combine multiple arrays

		console.log('Result from concat method: ');
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log(allTasks);

		//combine arrays with elements

		let combinedTasks = taskArrayA.concat('smell express', 'throw mongodb');
		console.log('Result form concat method: ');
		console.log(combinedTasks);

	//join
		/*
			Returns an array as a string sepearated by a specified separator
			Syntax

			arrayName.join('separatorString');
		*/

		let users = ['John', 'Jane', 'Joe', 'Robert', 'Neil'];

		console.log(users.join());
		console.log(users.join(' '));
		console.log(users.join(' - '));
		console.log(users.join(' * '));


//Iteration Method

/*
	Iteration methods are loops designed to perform repetitive tasks on arrays
	Iteration methods loops over all items in an array
	Useful for manipulating array data resulting in complex tasks
*/		

//forEach
/*
	similar to for loop that iterates on each array element
	-for each item in the array, the anonymous function passed in the forEach() method will be run
	-anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
	-variable names for arrays are normally written in the plural form of the data stored in an array
	-it's common practice to use the singular form of the array content for parameter names used in array loops
	-forEach() does NOT return anything
*/		

	allTasks.forEach(function(task){
		console.log(task);
	});	

	/*
		Mini Activity 2
		Create a function that can display ghostFighters one by one in our console
		Invoke the function
		Send a screenshot

	*/		
function displayGhostFighters(){
	ghostFighters.forEach(function(fighters){
		console.log(fighters);
	})
}

displayGhostFighters();

	//use forEach() with conditional statements

	let filteredTasks = [];

	allTasks.forEach(function(task){
		//console.log(task);

		//we 'filtered out' tasks with greater than 10 characters	
		if(task.length>10){
			console.log(task + " my length is greater than 10");
			//we added/push the 'tasks' filtered into the fileteredTasks array
			//it is no longer an empty array
			filteredTasks.push(task);
		}

	});

	console.log('Result of filteredTasks: ');
	console.log(filteredTasks);

	//map()

	/*
		iterates on each element and returns NEW ARRAY with different VALUES depending on the RESULT of the function's operation
	*/

	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(number){
		return number * number;
	})

	console.log("original Array: ");
	console.log(numbers);
	console.log("Results of map method: ");
	console.log(numberMap);

	//map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	});

	console.log(numberForEach);//does not return anything

	//every()
	/*
		check if all elements in an array meet the given condition 
	*/
	
	let allValid = numbers.every(function(number){
		return (number < 3);
	});

	console.log(allValid);//false

	//some()
	/*
		check if at least one element meet the given condition
	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	});

	console.log(someValid);//true

	//filter()

	let filterValid = numbers.filter(function(number){
		return (number < 3)
	});

	console.log(filterValid);

	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	})

	console.log(nothingFound);//[]

	
	//filtering using forEach
	let filteredNumbers = [];

	numbers.forEach(function(number){

		if(number < 3){
			filteredNumbers.push(number);
		}
	});

	console.log(filteredNumbers);

	//includes()

	/*
		returns a boolean which determines if an item is in the array or not
		returns true if the item is in the array
		returns false if the item is not in the array
	*/

	let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

	let productFound = products.includes('Mouse');
	console.log(productFound);//case sensitive///true

	let productNotFound = products.includes("Headset");
	console.log(productNotFound);//false

	//Method chaining

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	})

	console.log(filteredProducts);

//reduce()

	/*
		Syntax
		let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression/statement
		})
	*/

		console.log(numbers);
		let iteration = 0;

		let reducedArray = numbers.reduce(function(x,y){

			console.warn("Current iteration: " + ++iteration);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + y;
		});

		console.log("Result of reduce method: " + reducedArray);

		let iterationStr = 0;

		let list = ["Hello", "Again", "World"];

		let reduceJoin = list.reduce(function(x, y){
			console.warn("Current iteration: " + ++iterationStr);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + ' ' + y; 
		})
		console.log("Result of reduce method: " + reduceJoin);


		/*
		Mini Activity #3
		Create an addTrainer function that will enable us to add a trainer in the contacts array
		--This function should be able to receive a string
		--Determine if the added trainer already exists in the contacts array:
		--if it is, show an alert saying "Already added in the Match Call(Contacts)"
		--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
		--invoke and add a trainer in the browser's console
		--in the console, log the contacts array
		*/


		let contacts = ["Ash"];

		function addTrainer(trainer){

			let doesTrainerExist = contacts.includes(trainer);

			if(doesTrainerExist){
				alert("Already added in the Match Call.");
			}else{
				contacts.push(trainer);
				alert("Registered!");
			}

		}
		//addTrainer("Misty");
		//console.log(contacts);
